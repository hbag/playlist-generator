import flask
from flask import request
import pymysql.cursors, os, sys, requests, string, random, bcrypt, re, humanize, datetime
import datetime as dt
from datetime import date
from bs4 import BeautifulSoup
import http.cookiejar

from scrapetools import *

############################################
# generate playlist from provided threadid #
############################################

def playlist_actual(thread_id):
    id_array = []
    thread_title = "N/A"

    connection = pymysql.connect(
        host = os.getenv('HOSTIP'),
        user = os.getenv('DBUSER'),
        passwd = os.getenv('DBPASS')
    )

    cursor = connection.cursor()

    cursor.execute('USE playlist_gen')
    cursor.execute('SHOW TABLES')

    try:
        cursor.execute("SELECT thread_id FROM thread_vids WHERE thread_id=%(threadid)s;", {'threadid':thread_id})

        # if the thread exists in the DB, get its list of videos
        if cursor.fetchone() != None:
            # grab vid IDs
            cursor.execute('SELECT vid_ids FROM thread_vids WHERE thread_id=%(threadid)s;', {'threadid':thread_id})
            id_array = cursor.fetchone()[0].split(",")

            # greab thread title
            cursor.execute('SELECT thread_title FROM thread_vids WHERE thread_id=%(threadid)s', {'threadid':thread_id})
            thread_title = cursor.fetchone()[0]
        else:

            try:
                response = requests.get(f'https://api.fyad.club/threaddata/{thread_id}?token={os.getenv("PRIVTOKEN")}&page=1')
                response.raise_for_status()
                # access JSON content
                jsonResponse = response.json()
                if "error" not in jsonResponse:
                    thread_title = jsonResponse['thread_title']
                    for post in jsonResponse:
                        try:
                            videos = jsonResponse[f'{post}']['vids'].split(',')
                            for video in videos:
                                try:
                                    # extract video ID from URLs
                                    vid_id = video.split('/')[4].split('?')[0] # regex can eat my ass

                                    # if the ID isn't already in the array, add it
                                    if vid_id not in id_array:
                                        id_array.append(vid_id)
                                except IndexError:
                                    pass
                        except (KeyError, TypeError):
                            pass
            finally:
                pass

            # add thread to DB
            if "error" not in jsonResponse:

                # commit new thread to database
                cursor.execute('INSERT INTO thread_vids(thread_title,thread_id,vid_ids,last_page_viewed) VALUES (%(threadtitle)s,%(threadid)s,%(vidids)s,1)', {'threadtitle':jsonResponse['thread_title'],'threadid':thread_id,'vidids':','.join(id_array)})
                cursor.execute('COMMIT')

    finally:
        cursor.close()
        connection.close()
    
    resp = flask.make_response(flask.render_template('player.html', vid_ids=id_array, thread_title=thread_title))
    return resp