import flask
from flask import request
import pymysql.cursors, os, sys, requests, string, random, bcrypt, re
from datetime import date
from bs4 import BeautifulSoup
import http.cookiejar

from playlistshit import playlist_actual

app = flask.Flask(__name__)
scriptPath = os.path.dirname(sys.argv[0])

# routing
@app.route('/', methods=['GET','POST'])
def home():
    return flask.render_template('home.html')


@app.route('/playlist/<string:thread_id>', methods=['GET','POST'])
def playlist(thread_id):
    return playlist_actual(thread_id)