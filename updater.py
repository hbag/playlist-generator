import pymysql.cursors, sys, os, datetime, time, schedule
from scrapetools import *


def updateAll():
    connection = pymysql.connect(
        host = os.getenv('HOSTIP'),
        user = os.getenv('DBUSER'),
        passwd = os.getenv('DBPASS')
    )

    cursor = connection.cursor()

    cursor.execute('USE playlist_gen')
    cursor.execute('SHOW TABLES')

    playlist_array = []

    try:
        cursor.execute('SELECT thread_id FROM thread_vids;')
        playlist_array = cursor.fetchall()
    finally:
        cursor.close()
        connection.close()

    for playlist in playlist_array:
        dt_string = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print(f'[{dt_string}] - UPDATING PLAYLIST: {playlist[0]}')
        updatePlaylist(playlist[0])
    dt_string = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print(f'[{dt_string}] - FINISHED UPDATING PLAYLISTS')

starttime = time.time()
sleeptime = 3600
while True:
    updateAll()
    time.sleep(sleeptime - ((time.time() - starttime) % sleeptime))