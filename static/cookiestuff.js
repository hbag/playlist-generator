/////////////////////////////////////////////////////
//  most of the functions related to user cookies
//  - some will probably still be inline


// functions taken from w3schools
function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(cname) {
    let cookieName = getCookie(cname);
    if (cookieName != '') {
        return true;
    } else {
        return false;
    }
}

function removeFromArray(arr, value) {
    var index = arr.indexOf(value);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  }

  function hasImage(id) {
    var childElements = document.getElementById(id).childNodes;
    for (var i = 0; i < childElements.length; i++) {
        if (childElements[i].localName != null && childElements[i].localName.toLowerCase() == "img") {
            return true;
        }
    }
    return false;
}

// function to toggle flag bookmarks
function toggleFave(flagURL, flagUser, flagDate) {
    flagfullURL = flagURL;
    // strip the identical features from flag links to save space - cookies have a 4MB size limit
    flagURL = flagURL.replace('https://fi.somethingawful.com/flags/','').split('%20').join(' '); // horrid string formatting shit
    flagURL_Esc = flagURL.replace("'", "\\'");
    flagUser_Esc = flagUser.replace("'", "\\'");
    var flagFaved = false; // init flagFaved bool

    // check if 'favelist' cookie exists - if it doesn't, create it
    if (checkCookie('faveList') == false) {
        //setCookie('faveList', (flagURL + ','), 2100)
        setCookie('faveList', '', 2100)
    }
    // decode cookie into list of faved flags, iterate through them to check if the clicked flag is already bookmarked
    faveFlags = getCookie('faveList').split(',');
    for (let i = 0; i < faveFlags.length; i++) {
        if (faveFlags[i].replace(' ', '%20') == flagURL.replace(' ', '%20')) {flagFaved = true;}
    }

    // if a flag isn't bookmarked, bookmark it
    if (!flagFaved) {
        setCookie('faveList', (getCookie('faveList') + flagURL + ','));
        if (!hasImage('favflags')) { // if there's no images, clear the "nothing" shit and replace it w/ the flag
            document.getElementById('emptyhead').remove();
            document.getElementById('emptybody').remove();
        } else { // otherwise, remove the faveseperator that'll be there
            document.getElementById('faveseperator').remove();
        } // add the actual flag itself, and then re-add the faveseperator
        // look, for some reason if you change the 's to "s the entire thing shits itself and dies - i have no idea why.
        document.getElementById('favflags').innerHTML += ('<img id="' + flagfullURL + '" src="' + 
        flagfullURL + '" title="This flag proudly brought to you by ' + flagUser + ' on ' + flagDate + 
        '" onerror="reloadImage(this)" onclick="toggleFave(\'' + flagURL_Esc + '\', \'' + flagUser_Esc + '\', \'' + flagDate + '\');"> <br id="faveseperator"/>');

    // if it is, unbookmark it (every part of this 'else' statement is an affront to god)
    } else {
        // verify choice
        if (!confirm('Remove this flag from your bookmarks?')) {
            return;
        } else {
            removeFromArray(faveFlags,flagURL);
            setCookie('faveList', (faveFlags.join()))
            document.getElementById(('https://fi.somethingawful.com/flags/' + flagURL.split(' ').join('%20'))).remove();
            // if the fav modal has no flags in it, replace its contents with the "nothing" shit - you get the idea
            if (!hasImage('favflags')) {
                document.getElementById('faveseperator').remove();
                document.getElementById('favflags').innerHTML += 
                ('<h1 id="emptyhead">nothing.</h1><p id="emptybody">you haven\'t bookmarked any flags yet<br/>click a flag to toggle its bookmark');
            }
        }
    }
}
