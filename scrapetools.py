import pymysql.cursors, os
from requests.exceptions import HTTPError
import requests

def updatePlaylist(thread_id):
    #id_array = []

    connection = pymysql.connect(
        host = os.getenv('HOSTIP'),
        user = os.getenv('DBUSER'),
        passwd = os.getenv('DBPASS')
    )

    cursor = connection.cursor()

    cursor.execute('USE playlist_gen')
    cursor.execute('SHOW TABLES')

    try:
        # grab last checked page from DB
        cursor.execute('SELECT last_page_viewed FROM thread_vids WHERE thread_id=%(threadid)s', {'threadid':thread_id})
        last_checked = int(cursor.fetchone()[0])

        # find total # of pages in thread
        response = requests.get(f'https://api.fyad.club/threaddata/{thread_id}?token={os.getenv("PRIVTOKEN")}')
        response.raise_for_status()
        # access JSON content
        jsonResponse = response.json()
        thread_pages = int(jsonResponse['total_pages'])

        if last_checked <= thread_pages:
            # get videos ALREADY stored
            cursor.execute('SELECT vid_ids FROM thread_vids WHERE thread_id=%(threadid)s', {'threadid':thread_id})
            id_array = cursor.fetchone()[0].split(',')

            if (thread_pages - last_checked) < 150:
                scrape_until = thread_pages+1
            else:
                scrape_until = last_checked +150

            for i in range(last_checked-1, scrape_until):
                # find new vids
                response = requests.get(f'https://api.fyad.club/threaddata/{thread_id}?token={os.getenv("PRIVTOKEN")}&page={i}')
                response.raise_for_status()
                # access JSON content
                jsonResponse = response.json()

                for post in jsonResponse: #LOOPS WITHIN LOOPS INTERLINKED
                    try:
                        videos = jsonResponse[f'{post}']['vids'].split(',')
                        for video in videos:
                            try:
                                # extract video ID from URLs
                                vid_id = video.split('/')[4].split('?')[0] # regex can eat my ass

                                # if the ID isn't already in the array, add it
                                if vid_id not in id_array:
                                    id_array.append(vid_id)
                            except IndexError:
                                pass
                    except (KeyError, TypeError):
                        pass
                
                cursor.execute('UPDATE thread_vids SET thread_title = %(threadtitle)s, vid_ids = %(vidids)s, last_page_viewed = %(lastpage)s WHERE thread_id=%(threadid)s;', {'threadtitle':jsonResponse['thread_title'],'vidids':','.join(id_array), 'lastpage':i,'threadid':thread_id})
                cursor.execute('COMMIT')
        else:
            cursor.execute('UPDATE thread_vids SET thread_title = %(threadtitle)s WHERE thread_id=%(threadid)s;', {'threadtitle':jsonResponse['thread_title'],'threadid':thread_id})
            cursor.execute('COMMIT')

    except (HTTPError, requests.exceptions.ChunkedEncodingError):
        pass

    finally:
        cursor.close()
        connection.close()